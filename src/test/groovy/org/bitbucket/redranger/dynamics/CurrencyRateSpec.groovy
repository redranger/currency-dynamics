package org.bitbucket.redranger.dynamics

import grails.test.mixin.TestFor
import spock.lang.Specification

/**
 * See the API for {@link grails.test.mixin.domain.DomainClassUnitTestMixin} for usage instructions
 */
@TestFor(CurrencyRate)
class CurrencyRateSpec extends Specification {

    void 'test currency cannot be null'() {
        when:
        domain.currency = null

        then:
        !domain.validate(['currency'])
        domain.errors['currency'].code == 'nullable'
    }

    void 'test rateDate cannot be null'() {
        when:
        domain.rateDate = null

        then:
        !domain.validate(['rateDate'])
        domain.errors['rateDate'].code == 'nullable'
    }

    void 'test rateValue cannot be null'() {
        when:
        domain.rateValue = null

        then:
        !domain.validate(['rateValue'])
        domain.errors['rateValue'].code == 'nullable'
    }
}
