package org.bitbucket.redranger.dynamics

import grails.test.mixin.Mock
import grails.test.mixin.TestFor
import org.joda.time.DateTime
import spock.lang.Shared
import spock.lang.Specification

/**
 * See the API for {@link grails.test.mixin.services.ServiceUnitTestMixin} for usage instructions
 */
@TestFor(CurrencyDynamicsService)
@Mock([Currency, CurrencyRate])
class CurrencyDynamicsServiceSpec extends Specification {

    @Shared
    def date

    @Shared
    def testHtml

    def setupSpec() {
        date = new DateTime().withTimeAtStartOfDay()
        testHtml = "<tr><td>344</td>\n" +
                "<td>HKD</td>\n" +
                "<td>10</td>\n" +
                "<td>Гонконгских долларов</td>\n" +
                "<td>75,4713</td></tr>\n" +
                "<tr><td>208</td>\n" +
                "<td>DKK</td>\n" +
                "<td>10</td>\n" +
                "<td>Датских крон</td>\n" +
                "<td>92,2420</td></tr>\n" +
                "<tr><td>840</td>\n" +
                "<td>USD</td>\n" +
                "<td>1</td>\n" +
                "<td>Доллар США</td>\n" +
                "<td>58,9325</td></tr>\n" +
                "<tr><td>978</td>\n" +
                "<td>EUR</td>\n" +
                "<td>1</td>\n" +
                "<td>Евро</td>\n" +
                "<td>68,6623</td></tr>\n" +
                "<tr><td>356</td>\n" +
                "<td>INR</td>\n" +
                "<td>100</td>\n" +
                "<td>Индийских рупий</td>\n" +
                "<td>91,5883</td></tr>\n" +
                "<tr><td>398</td>\n" +
                "<td>KZT</td>\n" +
                "<td>100</td>\n" +
                "<td>Казахстанских тенге</td>\n" +
                "<td>18,1244</td></tr>"
    }

    void "test parseRateForCurrency"() {
        setup:
        def usd = new Currency(code: '840', name: 'USD')
        def eur = new Currency(code: '978', name: 'EUR')
        def wrong = new Currency(code: '000', name: 'WRG')
        def ratesSource = testHtml

        when: 'search usd rate'
        def usdRate = service.parseRateForCurrency(ratesSource, usd)
        then: 'returns correct rate'
        noExceptionThrown()
        usdRate.scale() == 4
        usdRate.precision() == 6

        when: 'search eur rate'
        def eurRate = service.parseRateForCurrency(ratesSource, eur)
        then: 'returns correct rate'
        noExceptionThrown()
        eurRate.scale() == 4
        eurRate.precision() == 6

        when: 'search rate that is not exists'
        def wrongRate = service.parseRateForCurrency(ratesSource, wrong)
        then: 'returns null, an exception has risen'
        thrown ParseRateException
        !wrongRate
    }

    void "test getRateOnDate URL"() {
        setup:
        def usd = new Currency(code: '840', name: 'USD')

        when: 'URL is null'
        def rate = service.getRateOnDate(date, usd, null)
        then: 'returns null, no exception has risen'
        noExceptionThrown()
        !rate

        when: 'a protocol is wrong'
        rate = service.getRateOnDate(date, usd, "htt://wrng.ptcl")
        then: 'returns null, no exception has risen'
        noExceptionThrown()
        !rate

        when:
        rate = service.getRateOnDate(date, usd, "$CurrencyDynamicsService.CBR_RATES_URL=wrong=${date.toString(CurrencyDynamicsService.CBR_DATE_PATTERN)}")
        then: 'returns null, no exception has risen'
        noExceptionThrown()
        !rate

        when: 'URL is correct'
        rate = service.getRateOnDate(date, usd, "$CurrencyDynamicsService.CBR_RATES_URL${date.toString(CurrencyDynamicsService.CBR_DATE_PATTERN)}")
        then: 'returns correct rate'
        noExceptionThrown()
        rate != null
        rate.currency == usd
        rate.rateDate == date.toDate()
        rate.rateValue != null

    }

    void "test getRateOnDate new rate creation"() {
        setup:
        def usd = new Currency(code: '840', name: 'USD')
        def ratesSourceUrl = "$CurrencyDynamicsService.CBR_RATES_URL${date.toString(CurrencyDynamicsService.CBR_DATE_PATTERN)}"
        def oldRate = CurrencyRate.findByRateDateAndCurrency(((DateTime) date).toDate(), usd)
        oldRate?.delete()

        when: 'there is no rate in DB'
        def rate = service.getRateOnDate(date, usd, ratesSourceUrl)
        then: 'returns correct rate stored in DB'
        noExceptionThrown()
        rate != null
        CurrencyRate.findByRateDateAndCurrency(((DateTime) date).toDate(), usd) == rate
    }

    void "test getRateOnDate currency is null"() {
        when: 'currency is null'
        def rate = service.getRateOnDate(date, null, null)
        then: 'returns null, an exception has risen'
        thrown LostCurrencyException
        !rate
    }

    void "test getRateOnDate date is null"() {
        setup:
        def usd = new Currency(code: '840', name: 'USD')
        when: 'date is null'
        def rate = service.getRateOnDate(null, usd, null)
        then: 'returns null, an exception has risen'
        thrown RuntimeException
        !rate
    }

    def "test getDynamicsData"() {
        setup:
        def currentDate = new DateTime().withTimeAtStartOfDay()
        def startDate = currentDate.minusDays(2)
        def currencies = []
        currencies << new Currency(code: '840', name: 'USD')
        currencies << new Currency(code: '978', name: 'EUR')

        when: 'all conditions are correct'
        def rates = service.getRatesForPeriod(startDate, currentDate, currencies)
        then: 'returns a list of correct rates'
        rates != null
        rates instanceof List<Map>
        !rates.isEmpty()
    }
}
