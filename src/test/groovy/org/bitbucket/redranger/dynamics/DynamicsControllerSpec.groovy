package org.bitbucket.redranger.dynamics

import grails.test.mixin.Mock
import grails.test.mixin.TestFor
import org.joda.time.DateTime
import spock.lang.Shared
import spock.lang.Specification

/**
 * See the API for {@link grails.test.mixin.web.ControllerUnitTestMixin} for usage instructions
 */
@TestFor(DynamicsController)
@Mock([Currency, CurrencyRate, CurrencyDynamicsService])
class DynamicsControllerSpec extends Specification {

    @Shared
    def rates

    def setupSpec() {
        rates = [[date: '21.10.2018', usd: '55.5555', eur: '66.6666'],
                 [date: '22.10.2018', usd: '54.4444', eur: '67.7777'],
                 [date: '23.10.2018', usd: '55.0000', eur: '66.0000']]
    }

    void "test getTableContent"() {

        when: 'rates are correct'
        def htmlBlock = controller.getTableContent(rates)

        then: 'returns a correct html block'
        htmlBlock != null
        htmlBlock instanceof String
        htmlBlock.startsWith('\n<tr><td>')
        htmlBlock.endsWith('</td></tr>\n')

        def htmlLines = htmlBlock.split('</tr>')

        //usd should be marked as maxValue
        def firstRate = htmlLines[0]
        firstRate.indexOf('maxValueCell') != -1
        firstRate.indexOf('minValueCell') == -1
        firstRate.substring(firstRate.indexOf('maxValueCell'), firstRate.indexOf(rates.get(0).usd)) == 'maxValueCell\'>'

        //eur should be marked as maxValue
        //usd should be marked as minValue
        def secondRate = htmlLines[1]
        secondRate.indexOf('maxValueCell') != -1
        secondRate.indexOf('minValueCell') != -1
        secondRate.substring(secondRate.indexOf('maxValueCell'), secondRate.indexOf(rates.get(1).eur)) == 'maxValueCell\'>'
        secondRate.substring(secondRate.indexOf('minValueCell'), secondRate.indexOf(rates.get(1).usd)) == 'minValueCell\'>'

        //eur should be marked as minValue
        def thirdRate = htmlLines[2]
        thirdRate.indexOf('maxValueCell') == -1
        thirdRate.indexOf('minValueCell') != -1
        thirdRate.substring(thirdRate.indexOf('minValueCell\'>'), thirdRate.indexOf(rates.get(2).eur)) == 'minValueCell\'>'
    }

    void "test getStartDateForChart"() {
        when:
        def date = controller.getStartDateForChart(rates)

        then:
        date instanceof List
        !date.isEmpty()
        date.get(0) == '21'
        date.get(1) == '10'
        date.get(2) == '2018'
    }

    void "test table"() {
        setup:
        views['/dynamics/_tableTop.gsp'] = '_tableTop template'
        views['/dynamics/_tableBottom.gsp'] = '_tableBottom template'
        new Currency(code: '840', name: 'USD').save(flush: true)
        new Currency(code: '978', name: 'EUR').save(flush: true)

        when:
        controller.table()

        then:
        response.text.startsWith('_tableTop template')
        response.text.endsWith('_tableBottom template')
        response.text.contains('\n<tr><td>')
        response.text.contains('</td></tr>\n')
    }

    void "test chart"() {
        setup:
        def currentDate = new DateTime().withTimeAtStartOfDay()
        def startDate = currentDate.minusMonths(1)
        new Currency(code: '840', name: 'USD').save(flush: true)
        new Currency(code: '978', name: 'EUR').save(flush: true)


        when:
        controller.chart()

        then:
        model.startDate instanceof List
        Integer.valueOf((String) model.startDate.get(0)) == startDate.getDayOfMonth()
        Integer.valueOf((String) model.startDate.get(1)) == startDate.getMonthOfYear()
        Integer.valueOf((String) model.startDate.get(2)) == startDate.getYear()

        model.usdRates instanceof List<BigDecimal>
        model.eurRates instanceof List<BigDecimal>
    }
}
