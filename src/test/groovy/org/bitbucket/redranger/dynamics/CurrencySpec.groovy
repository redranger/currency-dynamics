package org.bitbucket.redranger.dynamics

import grails.test.mixin.TestFor
import spock.lang.Specification

/**
 * See the API for {@link grails.test.mixin.domain.DomainClassUnitTestMixin} for usage instructions
 */
@TestFor(Currency)
class CurrencySpec extends Specification {

    void 'test code cannot be null'() {
        when:
        domain.code = null

        then:
        !domain.validate(['code'])
        domain.errors['code'].code == 'nullable'
    }

    void 'test code cannot be blank'() {
        when:
        domain.code = ''

        then:
        !domain.validate(['code'])
    }

    void 'test code can have a maximum of 255 characters'() {
        when: 'for a string of 256 characters'
        String str = 'a' * 256
        domain.code = str

        then: 'code validation fails'
        !domain.validate(['code'])
        domain.errors['code'].code == 'maxSize.exceeded'

        when: 'for a string of 256 characters'
        str = 'a' * 255
        domain.code = str

        then: 'code validation passes'
        domain.validate(['code'])
    }

    void 'test name cannot be null'() {
        when:
        domain.name = null

        then:
        !domain.validate(['name'])
        domain.errors['name'].code == 'nullable'
    }

    void 'test name cannot be blank'() {
        when:
        domain.name = ''

        then:
        !domain.validate(['name'])
    }

    void 'test name can have a maximum of 255 characters'() {
        when: 'for a string of 256 characters'
        String str = 'a' * 256
        domain.name = str

        then: 'name validation fails'
        !domain.validate(['name'])
        domain.errors['name'].code == 'maxSize.exceeded'

        when: 'for a string of 256 characters'
        str = 'a' * 255
        domain.name = str

        then: 'name validation passes'
        domain.validate(['name'])
    }
}
