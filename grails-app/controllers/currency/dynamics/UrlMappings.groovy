package currency.dynamics

class UrlMappings {

    static mappings = {
        "/$controller/$action?/$id?(.$format)?"{
            constraints {
                // apply constraints here
            }
        }

        "/"(controller: "Dynamics")
        "500"(view:'/error')
        "404"(view:'/notFound')
    }
}
