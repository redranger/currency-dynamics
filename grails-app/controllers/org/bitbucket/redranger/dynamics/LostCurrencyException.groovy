package org.bitbucket.redranger.dynamics

class LostCurrencyException extends RuntimeException {
    LostCurrencyException(String s) {
        super(s)
    }
}
