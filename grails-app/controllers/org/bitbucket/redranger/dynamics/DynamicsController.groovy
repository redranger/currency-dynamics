package org.bitbucket.redranger.dynamics

import grails.transaction.Transactional
import org.joda.time.DateTime

@Transactional(readOnly = true)
class DynamicsController {

    public static def CURRENCY_CODES = ['840', '978']

    def currencyDynamicsService
    static defaultAction = "table"

    def table() {
        def currencies = Currency.findAllByCodeInList(CURRENCY_CODES)
        def currentDate = new DateTime().withTimeAtStartOfDay()
        def startDate = currentDate.minusMonths(1)
        def rates = currencyDynamicsService.getRatesForPeriod(startDate, currentDate, currencies)

        def tableContent = getTableContent(rates)

        render(template: "tableTop")
        render text: tableContent, contentType: "text/html", encoding: "UTF-8"
        render(template: "tableBottom")

    }

    /**
     * Prepares a html-code block (a table body) with rates
     * @param rates - a list of rates
     * @return string of html-content
     */
    def getTableContent(rates) {
        def maxUsd = rates.max { it.usd }
        def maxEur = rates.max { it.eur }
        def minUsd = rates.min { it.usd }
        def minEur = rates.min { it.eur }

        def tableContent = new StringBuilder()
        rates.each { rate ->
            tableContent.append("\n<tr><td>${rate.date}</td>")
            tableContent.append(rate == maxUsd ? "<td class='maxValueCell'>" : rate == minUsd ? "<td class='minValueCell'>" : "<td>")
            tableContent.append("${rate.usd}</td>")
            tableContent.append(rate == maxEur ? "<td class='maxValueCell'>" : rate == minEur ? "<td class='minValueCell'>" : "<td>")
            tableContent.append("${rate.eur}</td>")
            tableContent.append("</tr>")
        }
        tableContent.append("\n")
        tableContent.toString()
    }

    def chart() {
        def currencies = Currency.findAllByCodeInList(CURRENCY_CODES)
        def currentDate = new DateTime().withTimeAtStartOfDay()
        def startDate = currentDate.minusMonths(1)
        def rates = currencyDynamicsService.getRatesForPeriod(startDate, currentDate, currencies)

        def parsedStartDate = getStartDateForChart(rates)
        def usdRates = rates*.usd
        def eurRates = rates*.eur

        respond([startDate: parsedStartDate,
                 usdRates : usdRates,
                 eurRates : eurRates])

    }

    /**
     * Takes first rate from list and parses date to a 3-element list.<br>
     * For example, date "22.05.2025" will be separated into [ "22", "05", "2025"]
     * @param rates - a list of rates
     * @return list of strings representing date
     */
    def getStartDateForChart(rates) {
        def startDate = (String) rates.first()?.date
        return startDate.tokenize('.')
    }
}
