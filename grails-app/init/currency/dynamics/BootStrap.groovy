package currency.dynamics

import org.bitbucket.redranger.dynamics.Currency

class BootStrap {

    def init = { servletContext ->
        new Currency(code: '840', name: 'USD').save()
        new Currency(code: '978', name: 'EUR').save()
    }
    def destroy = {
    }
}
