package org.bitbucket.redranger.dynamics

import grails.transaction.Transactional
import org.joda.time.DateTime

@Transactional
class CurrencyDynamicsService {

    public static String CBR_RATES_URL = 'https://www.cbr.ru/currency_base/daily.aspx?date_req='
    public static String CBR_DATE_PATTERN = 'dd.MM.yyyy'

    /**
     * Collects and represents rate data over last month as list
     * @return list of {@link Map}
     */
    def getRatesForPeriod(startDate, finishDate, currencies) {
        def ratesList = new ArrayList<Map>()

        while (finishDate > startDate) {
            try {
                def dailyRate = [:]
                def sourcePageUrl = "$CBR_RATES_URL${startDate.toString(CBR_DATE_PATTERN)}"

                currencies.each { currency ->
                    try {
                        def rate = getRateOnDate(startDate, currency, sourcePageUrl)
                        dailyRate[currency.name.toLowerCase()] = rate?.rateValue
                    } catch (Exception e) {
                        log.error("Error during getting the rate of ${currency.name} on $startDate", e)
                    }
                }

                dailyRate.date = startDate.toString(CBR_DATE_PATTERN)
                ratesList << dailyRate
            } catch (Exception e) {
                log.error("Error during getting the rate on $startDate", e)
            } finally {
                startDate = startDate.plusDays(1)
            }
        }
        ratesList
    }

    /**
     * Searches a currency rate in DB by date and currency. If the rate hasn't found takes a new data and save it in DB.
     * @param date rate date
     * @param currency {@link Currency}
     * @param sourcePageUrl URL string to source page with rates
     * @return {@link CurrencyRate}
     * @exception LostCurrencyException if currency is {@code null}.
     */
    def getRateOnDate(DateTime date, Currency currency, String sourcePageUrl) throws RuntimeException {

        if (!currency) {
            throw new LostCurrencyException("Currency for getting rate is null")
        }

        if (!date) {
            throw new RuntimeException("Date must be provided to search rate")
        }

        def rate = CurrencyRate.findByRateDateAndCurrency(date.toDate(), (Currency) currency)

        if (!rate) {
            try {
                def ratesSource = new URL(sourcePageUrl).getText()
                def newRate = parseRateForCurrency(ratesSource, currency)

                rate = new CurrencyRate(currency: currency,
                        rateDate: date.toDate(),
                        rateValue: newRate)
                rate.save()
            } catch (MalformedURLException e) {
                log.error("Error during getting the rate from $sourcePageUrl", e)
            } catch (ParseRateException prex) {
                log.error("Error during parsing the data from source page", prex)
            } catch (Exception ex) {
                log.error("Error during getting the rate on $date", ex)
            }
        }
        return rate
    }

    /**
     * Takes substring with rate for currency from source html-text
     * @param ratesSource string html-text
     * @param currency {@link Currency}
     * @return {@link BigDecimal}
     * @exception ParseRateException
     */
    def parseRateForCurrency(String ratesSource, Currency currency) throws ParseRateException {
        try {
            def startOfRateLine = ratesSource.substring(ratesSource.indexOf(currency.code), ratesSource.length())
            def rateLine = startOfRateLine.substring(0, startOfRateLine.indexOf('</td></tr>'))
            def value = rateLine.substring(rateLine.length() - 7).replace(',', '.')
            return new BigDecimal(value)
        } catch (Exception e) {
            throw new ParseRateException(e.message)
        }
    }
}
