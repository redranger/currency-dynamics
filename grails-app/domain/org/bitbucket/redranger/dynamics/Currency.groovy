package org.bitbucket.redranger.dynamics

class Currency {

    String code
    String name

    static constraints = {
        code blank: false, unique: true, maxSize: 255
        name blank: false, unique: true, maxSize: 255
    }
}
