package org.bitbucket.redranger.dynamics

class CurrencyRate {

    Currency currency
    Date rateDate
    BigDecimal rateValue

    static constraints = {
        rateValue scale: 4, min: BigDecimal.ZERO
    }
}
