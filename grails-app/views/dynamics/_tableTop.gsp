<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="description" content="">
    <meta name="author" content="">

    <asset:stylesheet src="bootstrap.css"/>
    <asset:stylesheet src="custom.css"/>

    <title>Currency dynamics</title>
</head>

<body>

<nav class="navbar navbar-default navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <a class="navbar-brand" href="#">Currency dynamics</a>
        </div>

        <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
                <li class="active"><a href="#">Table</a></li>
                <li><g:link controller="dynamics" action="chart">Chart</g:link></li>
            </ul>
        </div>
    </div>
</nav>

<div class="container">

    <div class="jumbotron">
        <h1>Table</h1>

        <p>A table filled with daily currency rates over last month.</p>

        <table class="table">
            <thead class="thead-inverse">
            <tr>
                <th>Date</th>
                <th>USD</th>
                <th>EUR</th>
            </tr>
            </thead>
            <tbody>