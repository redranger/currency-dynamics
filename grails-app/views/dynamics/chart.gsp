<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="description" content="">
    <meta name="author" content="">

    <asset:stylesheet src="bootstrap.css"/>

    <asset:javascript src="jquery-2.2.0.min.js"/>
    <asset:javascript src="shieldui-all.min.js"/>%{--shieldui.com--}%

    <title>Currency dynamics</title>
</head>

<body>

<nav class="navbar navbar-default navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <a class="navbar-brand" href="#">Currency dynamics</a>
        </div>

        <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
                <li><g:link controller="dynamics" action="table">Table</g:link></li>
                <li class="active"><a href="#">Chart</a></li>
            </ul>
        </div>
    </div>
</nav>

<div class="container">

    <div class="jumbotron">
        <h1>Chart</h1>

        <p>A chart reflecting the currency dynamics over last month.</p>

        <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2">

                    <div id="chart"></div>

                    <script>
                        $(function () {

                            $("#chart").shieldChart({
                                theme: "bootstrap",
                                exportOptions: {
                                    image: false,
                                    print: false
                                },
                                primaryHeader: {
                                    text: "Currency dynamics"
                                },
                                axisX: {
                                    axisType: 'datetime',
                                    axisTickText: {
                                        y: 30,
                                        textAngle: 300
                                    },
                                },
                                axisY: {
                                    axisTickText: {
                                        format: function(text, axis, chart) {
                                            return "\u20BD " + text;
                                        }
                                    }
                                },
                                dataSeries: [{
                                    seriesType: 'line',
                                    collectionAlias: "USD",
                                    dataStart: Date.UTC(${startDate.get(2)}, ${startDate.get(1)} - 1, ${startDate.get(0)}),
                                    dataStep: 24 * 3600 * 1000, // one day
                                    data: ${usdRates}
                                }, {
                                    seriesType: 'line',
                                    collectionAlias: "EUR",
                                    dataStart: Date.UTC(${startDate.get(2)}, ${startDate.get(1)} - 1, ${startDate.get(0)}),
                                    dataStep: 24 * 3600 * 1000, // one day
                                    data: ${eurRates}
                                }]
                            });
                        });
                    </script>

                </div>
            </div>
        </div>

    </div>

</div>

</body>
</html>
